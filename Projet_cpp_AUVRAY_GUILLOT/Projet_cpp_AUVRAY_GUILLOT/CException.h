#ifndef EXC
#define EXC 50

/*!
* \file CException.h
* \brief Generateur d'exeption
* \author {GUILLOT Donovan, AUVRAY, Damien}
* \version 1
*/

#include <string>
#include <iostream>  
using namespace std;

/** \class CException
* \brief Genere une exeption
*/
class CException {
	/** Attributs */
private:
	unsigned int uiEXCValeur; /*!< Valeur de l'exception*/
	std::string sEXCMessage; /*!< contenue du message d'exception*/
public:
	/*!
	*  \brief Constructeur
	*  Constructeur de la classe CException
	*/
	CException();

	/*!
	*  \brief Constructeur
	*  Constructeur de la classe CException
	*  \param uiValeur : valeur de l'exception
	*  \param sMessage : contenue du message d'exception
	*/
	CException(unsigned int uiValeur, std::string sMessage);

	/*!
	*  \brief Destructeur
	*  Destructeur de la classe CException
	*/
	~CException();

	/** m�thodes */

	/*!
	*	\brief lit l'attribut valeur de la classe
	*	\return int : la valeur de l'excetpion
	*/
	unsigned int EXCLireValeur();

	/*!
	*	\brief modifie l'attribut valeur de la classe
	*	\param uiValeur : valeur de l'exception
	*/
	void EXCModifierValeur(unsigned int uiValeur);

	/*!
	*	\brief lit l'attribut valeur de la classe
	*	\return string : le contenue du message de l'excetpion
	*/
	std::string EXCLireMessage();

	/*!
	*	\brief modifie l'attribut message de la classe
	*	\param sMessage : le contenue du message de l'excetpion
	*/
	void EXCModifierMessage(std::string sMessage);

	/*!
	*	\brief Surcharge de l'op�rateur <<
	*	\param OSTflux : flux de donn�e ostream
	*	\param EXCException : exception a afficher
	*/
	friend ostream& operator<<(ostream& OSTflux, CException& EXCException);  
};
#endif