#ifndef MAT
#define MAT 50

/*!
* \file CMatrice.h
* \brief Fichier manipulant des matrices
* \author {GUILLOT Donovan, AUVRAY, Damien}
* \version 1
*/

#include <vector>
#include <iostream>

/** \class CMatrice
*	\brief création et manipulation de matrice
*/ 
template <class MType>
class CMatrice {

	/** Attributs */
private:
	std::vector<std::vector<MType>> vVCTVCTMTCMatrice; /*!< la matrice à minipuler, un vcteur de plusieurs vecteurs*/
public:
	/*!  \brief Constructeur par defaultn, ne fait rien */
	CMatrice(){}
	/*! \brief Constructeur de recopie
	*	\param MTCparam matrice à recopier
	*/
	CMatrice(CMatrice<MType> & MTCparam);
	/*! \brief Destructeur, ne fais rien */
	~CMatrice(){}

	/** Méthodes */

	/*!
	*	\brief modifie l'attribut matrice de la classe
	*	\param vVTCVTCVector : le vecteur de vecteurs à donner à l'objet
	*/
	void MTCModifierMatrice(std::vector<std::vector<MType>> vVTCVTCVector);

	/*!
	*	\brief lit l'attribut matrice de la classe
	*	\return std::vector<std::vector<MType>> : la matrice attibut de l'objet
	*/
	std::vector<std::vector<MType>> MTCLireMatrice();

	/*!
	*	\brief affiche la matrice dans la console
	*/
	void MTCAfficher();

	/*!
	*	\brief calcul la matrice transposée de l'objet en cours et la retourne
	*	\return CMatrice<MType> : la matrice transposee 
	*/
	CMatrice<MType> MTCTransposee();

	/*!
	*	\brief calcul le produit de l'objet en cours avec une constante et la retourne
	*	\param iConstante : la constante par laquelle on multiplie l'objet en cours
	*	\return CMatrice<MType> : la matrice resultat
	*/
	CMatrice<MType> operator* (MType MTPConstante);

	/*!
	*	\brief calcul le produit de l'objet en cours avec une autre matrice et la retourne
	*	\param MTCparam : la matrice par laquelle on multiplie l'objet en cours
	*	\return CMatrice<MType> : la matrice resultat
	*/
	CMatrice<MType> operator* (CMatrice<MType> & MTCparam);

	/*!
	*	\brief calcul le division de l'objet en cours avec une constante et la retourne
	*	\param iConstante : la constante par laquelle on divise l'objet en cours
	*	\return CMatrice<MType> : la matrice resultat
	*/
	CMatrice<MType> operator/ (MType MTPConstante);

	/*
	*	\brief calcul la somme de l'objet en cours avec une matrice et la retourne
	*	\param MTCparam : la matrice par laquelle on additionne l'objet en cours
	*	\return CMatrice<MType> : la matrice resultat
	*/
	CMatrice<MType> operator+ (CMatrice<MType> & MTCparam);

	/*!
	*	\brief calcul la différence de l'objet en cours avec une constante et la retourne
	*	\param MTCparam : la matrice par laquelle on soustrait l'objet en cours
	*	\return CMatrice<MType> : la matrice resultat
	*/
	CMatrice<MType> operator- (CMatrice<MType> & MTCparam);
};

#include "CMatrice.cpp"

#endif

