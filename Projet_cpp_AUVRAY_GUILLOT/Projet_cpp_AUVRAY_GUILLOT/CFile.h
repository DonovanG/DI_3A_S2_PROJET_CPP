#ifndef FIL
#define FIL 50

/*!
* \file CFile.h
* \brief Lecture d'une matrice dans un fichier
* \author {GUILLOT Donovan, AUVRAY, Damien}
* \version 1
*/

#include <iostream>
#include <fstream>
#include <string>
#include "CMatrice.h"

using namespace std;

/** \class CFile
* \brief CFile permer d'ouvrir un fichier  � partir de son nom et d'en extraire la matrice
*/
class CFile
{
	/** Attributs */
private :
	string FILNomFichier; /*!< le nom du fichier*/
    ifstream FILFichier; /*!< le stream de lecture li� au fichier*/

public:
	/*!
	*  \brief Constructeur
	*  Constructeur de la classe CFile
	*  \param filename : nom du fichier
	*/
    CFile(string filename);

	/** m�thodes */

	/*!
	* \brief Parse une ligne d'une matrice pour en faire un vecteur
	*\param sChaine : la chaine � parser en vecteur
	* \return vLigne : le vecteur correspondant a sChaine
	*/
    CMatrice<double> FILLireMatrice();

	/*!
	*  \brief  parse une ligne d'une matrice pour enn r�cup�rer lec vecteur
	*  \return MTCmatrice : la matrice correspondant au fichier
	*/
    vector<double> FILParserLigne(string sChaine);
};

#endif
