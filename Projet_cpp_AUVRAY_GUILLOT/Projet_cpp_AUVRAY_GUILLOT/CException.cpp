#include "CException.h"

/**
* \file CException.h
* \brief Generateur d'exeption
* \author {GUILLOT Donovan, AUVRAY, Damien}
* \version 1
*/

/*! \fn  CException::CException()
*  \brief Constructeur
*  Constructeur de la classe CException
*/
CException::CException(){
	uiEXCValeur=1;
	sEXCMessage="CException Erreur Generique";
}

/*! \fn CException::CException(unsigned int uiValeur, std::string sMessage)
*  \brief Constructeur
*  Constructeur de la classe CException
*  \param uiValeur : valeur de l'exception
*  \param sMessage : contenue du message d'exception
*/
CException::CException(unsigned int uiValeur, std::string sMessage){
	uiEXCValeur=uiValeur;
	sEXCMessage=sMessage;
}

/*! \fn CException::~CException()
*  \brief Destructeur
*  Destructeur de la classe CException
*/
CException::~CException(){
}

/*! \fn unsigned int CException::EXCLireValeur()
*	\brief lit l'attribut valeur de la classe
*	\return int : la valeur de l'excetpion
*/
unsigned int CException::EXCLireValeur(){
	return uiEXCValeur;
}

/*! \fn void CException::EXCModifierValeur(unsigned int uiValeur)
*	\brief modifie l'attribut valeur de la classe
*	\param uiValeur : valeur de l'exception
*/
void CException::EXCModifierValeur(unsigned int uiValeur){
	uiEXCValeur = uiValeur;
}

/*! \fn std::string CException::EXCLireMessage()
*	\brief lit l'attribut valeur de la classe
*	\return string : le contenue du message de l'excetpion
*/
std::string CException::EXCLireMessage(){
	return sEXCMessage;
}

/*! \fn void CException::EXCModifierMessage(std::string sMessage)
*	\brief modifie l'attribut message de la classe
*	\param sMessage : le contenue du message de l'excetpion
*/
void CException::EXCModifierMessage(std::string sMessage){
	sEXCMessage = sMessage;
}

/*! \fn ostream& operator<<(ostream& OSTflux, CException& EXCException)
*	\brief Surcharge de l'op�rateur <<
*	\param OSTflux : flux de donn�e ostream
*	\param EXCException : exception a afficher
*/
ostream& operator<<(ostream& OSTflux, CException& EXCException){
  OSTflux << "L'erreur levee est : "<< EXCException.EXCLireValeur() << " " << EXCException.EXCLireMessage() << endl;
  return OSTflux;
}