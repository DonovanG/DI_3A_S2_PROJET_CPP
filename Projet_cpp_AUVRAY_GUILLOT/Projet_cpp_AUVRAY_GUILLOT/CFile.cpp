/**
* \file CFile.cpp
* \brief Lecture d'une matrice dans un fichier
* \author{ GUILLOT Donovan, AUVRAY, Damien }
*\version 1
*/

#include "CFile.h"
#include "CException.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

/**	constructeur � partir du nom de fichier dans lequel on veut lire une matrice
* \param string filename : le nom du fichier
*/
CFile::CFile(string filename)
{
        FILNomFichier = filename;
        FILFichier = ifstream(FILNomFichier.c_str(), ios::in);
        if (!FILFichier) throw CException(6, "Erreur Ouverture du fichier");
        
}

/** \fn CMatrice<double> CFile::FILLireMatrice()
* \brief Lit une matrice dans le fichier instanci�
* \return CMatrice<double> MTCmatrice : la matrice de double extraite du fichier
*/
CMatrice<double> CFile::FILLireMatrice() {
	/** variable */
     string buffer;	/*!< buffer qui va lire ligne � ligne dans le fichier*/
    CMatrice<double> MTCmatrice; /*!< la matrice � retourner*/
    vector<vector<double>> vVecteur; /*!< le vecteur principale de la matrice*/
    vector<double> vSousVecteur; /*!< les sous vecteur du vecteure principal*/
    string sType; /*!< le type de la matrice*/
    unsigned int uiNblignes; /*!< le nombre de ligne de la matrice*/
    unsigned int uiNbColonnes; /*!< le nombre de colonne de la matricer*/


    /** lire de type du fichier */
        getline(FILFichier, buffer);
        sType = buffer.substr(buffer.find("=") + 1);
		/** lever d'une exception si le type n'est pas double*/
		if(sType!="double")
			throw CException(5, "Erreur les matrices doivent etre de type double");

        /**trouve le nombre de ligne � partir du fichir */
        getline(FILFichier, buffer);
        buffer = buffer.substr(buffer.find("=") + 1);
		if (sscanf_s(buffer.c_str(), "%d", &uiNblignes) != 1)
			throw CException(7, "Erreur lignes non valides");

        
        /**trouve le nombre de colonne � partir du fichir */
        getline(FILFichier, buffer);
        buffer = buffer.substr(buffer.find("=") + 1);
		if (sscanf_s(buffer.c_str(), "%d", &uiNbColonnes) != 1)
			throw CException(8, "Erreur colonnes non valides");


        /** saute la ligne "Matrice = [" du fichier texte */
        getline(FILFichier, buffer);

        /** lit la matrice dans le fichier */
        for (unsigned int uiBoucle1 = 0; uiBoucle1 < uiNblignes; uiBoucle1++) {
                getline(FILFichier, buffer);
                vSousVecteur = FILParserLigne(buffer);
                vVecteur.push_back(vSousVecteur);
                vSousVecteur.clear();
        }
        MTCmatrice.MTCModifierMatrice(vVecteur);

		/** fermeture du flux de donn�e en lecture */
        FILFichier.close();

        return MTCmatrice;
}

/** \fn vector<double> CFile::FILParserLigne(string sChaine) 
* \brief Parse une ligne d'une matrice pour en faire un vecteur
*\param sChaine : la chaine � parser en vecteur
* \return vLigne : le vecteur correspondant a sChaine
*/
vector<double> CFile::FILParserLigne(string sChaine) {

        string sDelimiter = " "; /*!< le delimiteur entre chaque element d'une ligne*/
        vector<double> vLigne; /*!< le vecteur � retourner*/
        string sLigne = sChaine; /*!< le clone de la chaine pass�e en param�tre*/
        string sElement = ""; /*!< l'instance d'un �l�ment d'une ligne en string */

        int position = 0; /*!< la position d'un d�limiteur dans la chaine */
        double dElement = 0.0; /*!< l'instance d'un element d'une ligne en double */

		/**parsage */
        while (position = sLigne.find(sDelimiter) != string::npos) {

                sElement = sLigne.substr(0, position);
                dElement = stod(sElement);
                vLigne.push_back(dElement);
                sLigne.erase(0, position + sDelimiter.length());
        }

        //ajout dernier �l�ment
        dElement = stod(sLigne);
        vLigne.push_back(dElement);

        return vLigne;
}