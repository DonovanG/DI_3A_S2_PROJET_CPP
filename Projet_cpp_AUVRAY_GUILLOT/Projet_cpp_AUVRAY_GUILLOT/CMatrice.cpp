/**
*	\file CMatrice.h
*	\brief Fichier manipulant des matrices
*	\author{ GUILLOT Donovan, AUVRAY, Damien }
*	\version 1
*/

/*! \fn  CMatrice<MType>::CMatrice(CMatrice<MType> & MTCparam)
*	\brief Constructeur de recopie
*	\param MTCparam matrice � recopier
*/
template<class MType>
CMatrice<MType>::CMatrice(CMatrice<MType> & MTCparam) {
	vVCTVCTMTCMatrice = std::vector<std::vector<MType>>(MTCparam.vVCTVCTMTCMatrice);
}

/*! \fn void CMatrice<MType>::MTCModifierMatrice(std::vector<std::vector<MType>> vVTCVTCVector)
*	\brief modifie l'attribut matrice de la classe
*	\param vVTCVTCVector : le vecteur de vecteurs � donner � l'objet
*/
template<class MType>
void CMatrice<MType>::MTCModifierMatrice(std::vector<std::vector<MType>> vVTCVTCVector) {
	vVCTVCTMTCMatrice = vVTCVTCVector;
}

/*! \fn std::vector<std::vector<MType>> CMatrice<MType>::MTCLireMatrice() 
*	\brief lit l'attribut matrice de la classe
*	\return std::vector<std::vector<MType>> : la matrice attibut de l'objet
*/
template<class MType>
std::vector<std::vector<MType>> CMatrice<MType>::MTCLireMatrice() {
	return vVCTVCTMTCMatrice;
}

/*! \fn void CMatrice<MType>::MTCAfficher()
*	\brief affiche la matrice dans la console
*/
template<class MType>
void CMatrice<MType>::MTCAfficher() {
	for (unsigned int uiBoucle1 = 0; uiBoucle1 < (unsigned)vVCTVCTMTCMatrice.size(); uiBoucle1++) {
		for (unsigned int uiBoucle2 = 0; uiBoucle2 < (unsigned)vVCTVCTMTCMatrice[0].size(); uiBoucle2++) {
			std::cout << vVCTVCTMTCMatrice[uiBoucle1][uiBoucle2] << " ";
		}
		std::cout << std::endl;
	}
}

/*! \fn CMatrice<MType> CMatrice<MType>::MTCTransposee()
*	\brief calcul la matrice transpos�e de l'objet en cours et la retourne
*	\return CMatrice<MType> : la matrice transposee
*/
template<class MType>
CMatrice<MType> CMatrice<MType>::MTCTransposee() {
	CMatrice<MType> MTCmatrice;
	vector<MType> vVTCVector;
	for (unsigned int uiBoucle1 = 0; uiBoucle1 < (unsigned)vVCTVCTMTCMatrice[0].size(); uiBoucle1++) {
		for (unsigned int uiBoucle2 = 0; uiBoucle2 < (unsigned)vVCTVCTMTCMatrice.size(); uiBoucle2++) {
			vVTCVector.push_back(vVCTVCTMTCMatrice[uiBoucle2][uiBoucle1]);
		}
		MTCmatrice.vVCTVCTMTCMatrice.push_back(vVTCVector);
		vVTCVector.clear();
	}
	return MTCmatrice;
}

/*! \fn CMatrice<MType> CMatrice<MType>::operator * (MType iConstante)
*	\brief calcul le produit de l'objet en cours avec une constante et la retourne
*	\param iConstante : la constante par laquelle on multiplie l'objet en cours
*	\return CMatrice<MType> : la matrice resultat
*/
template<class MType>
CMatrice<MType> CMatrice<MType>::operator * (MType MTPConstante) {
	CMatrice<MType> MTCmatrice(*this);
	for (unsigned int uiBoucle1 = 0; uiBoucle1 < vVCTVCTMTCMatrice.size(); uiBoucle1++) {
		for (unsigned int uiBoucle2 = 0; uiBoucle2 < vVCTVCTMTCMatrice[0].size(); uiBoucle2++) {
			MTCmatrice.vVCTVCTMTCMatrice[uiBoucle1][uiBoucle2] *= MTPConstante;
		}
	}
	return MTCmatrice;
}

/*! \fn CMatrice<MType> CMatrice<MType>::operator * (CMatrice<MType> & MTCparam)
*	\brief calcul le produit de l'objet en cours avec une autre matrice et la retourne
*	\param MTCparam : la matrice par laquelle on multiplie l'objet en cours
*	\return CMatrice<MType> : la matrice resultat
*/
template<class MType>
CMatrice<MType> CMatrice<MType>::operator * (CMatrice<MType> & MTCparam) {
	if(vVCTVCTMTCMatrice[0].size()!=MTCparam.vVCTVCTMTCMatrice.size())
		throw CException(1, "Erreur operateur *: matrices de tailles incompatibles");
	CMatrice<MType> MTCmatrice;
	vector<MType> vVTCVector;
	MType MTPResultat;
	for (unsigned int uiBoucle1 = 0; uiBoucle1 < vVCTVCTMTCMatrice.size(); uiBoucle1++){
		for (unsigned int uiBoucle2 = 0; uiBoucle2 < MTCparam.vVCTVCTMTCMatrice[0].size(); uiBoucle2++){
			MTPResultat = NULL;
			for(unsigned int uiBoucle3 = 0; uiBoucle3 < vVCTVCTMTCMatrice[0].size(); uiBoucle3++){
				MTPResultat += (vVCTVCTMTCMatrice[uiBoucle1][uiBoucle3] * MTCparam.vVCTVCTMTCMatrice[uiBoucle3][uiBoucle2]);
			}
			vVTCVector.push_back(MTPResultat);
		}
		MTCmatrice.vVCTVCTMTCMatrice.push_back(vVTCVector);
		vVTCVector.clear();
	}
	return MTCmatrice;
}

/*! \fn CMatrice<MType> CMatrice<MType>::operator / (MType iConstante)
*	\brief calcul le division de l'objet en cours avec une constante et la retourne
*	\param iConstante : la constante par laquelle on divise l'objet en cours
*	\return CMatrice<MType> : la matrice resultat
*/
template<class MType>
CMatrice<MType> CMatrice<MType>::operator / (MType MTPConstante) {
	if (MTPConstante == 0)
		throw CException(2, "Erreur Division par 0");
	CMatrice<MType> MTCmatrice(*this);
	for (unsigned int uiBoucle1 = 0; uiBoucle1 < vVCTVCTMTCMatrice.size(); uiBoucle1++) {
		for (unsigned int uiBoucle2 = 0; uiBoucle2 < vVCTVCTMTCMatrice[0].size(); uiBoucle2++) {
			MTCmatrice.vVCTVCTMTCMatrice[uiBoucle1][uiBoucle2] /= MTPConstante;
		}
	}
	return MTCmatrice;
}

/* \fn CMatrice<MType> CMatrice<MType>::operator+ (CMatrice<MType> & MTCparam)
*	\brief calcul la somme de l'objet en cours avec une matrice et la retourne
*	\param MTCparam : la matrice par laquelle on additionne l'objet en cours
*	\return CMatrice<MType> : la matrice resultat
*/
template<class MType>
CMatrice<MType> CMatrice<MType>::operator+ (CMatrice<MType> & MTCparam) {
	if(vVCTVCTMTCMatrice.size()!=MTCparam.vVCTVCTMTCMatrice.size() || vVCTVCTMTCMatrice[0].size()!=MTCparam.vVCTVCTMTCMatrice[0].size())
		throw CException(3, "Erreur operateur +: matrices de tailles differentes");
	CMatrice<MType> MTCmatrice(*this);
	for (unsigned int uiBoucle1 = 0; uiBoucle1 < vVCTVCTMTCMatrice.size(); uiBoucle1++) {
		for (unsigned int uiBoucle2 = 0; uiBoucle2 < vVCTVCTMTCMatrice[0].size(); uiBoucle2++) {
			MTCmatrice.vVCTVCTMTCMatrice[uiBoucle1][uiBoucle2] += MTCparam.vVCTVCTMTCMatrice[uiBoucle1][uiBoucle2];
		}
	}
	return MTCmatrice;
}

/*!	\fn CMatrice<MType> CMatrice<MType>::operator- (CMatrice<MType> & MTCparam)
*	\brief calcul la diff�rence de l'objet en cours avec une constante et la retourne
*	\param MTCparam : la matrice par laquelle on soustrait l'objet en cours
*	\return CMatrice<MType> : la matrice resultat
*/
template<class MType>
CMatrice<MType> CMatrice<MType>::operator- (CMatrice<MType> & MTCparam) {
	if(vVCTVCTMTCMatrice.size()!=MTCparam.vVCTVCTMTCMatrice.size() || vVCTVCTMTCMatrice[0].size()!=MTCparam.vVCTVCTMTCMatrice[0].size())
		throw CException(4, "Erreur operateur -: matrices de tailles differentes");
	CMatrice<MType> MTCmatrice (*this);
	for (unsigned int uiBoucle1 = 0; uiBoucle1 < vVCTVCTMTCMatrice.size(); uiBoucle1++) {
		for (unsigned int uiBoucle2 = 0; uiBoucle2 < vVCTVCTMTCMatrice[0].size(); uiBoucle2++) {
			MTCmatrice.vVCTVCTMTCMatrice[uiBoucle1][uiBoucle2] -= MTCparam.vVCTVCTMTCMatrice[uiBoucle1][uiBoucle2];
		}
	}
	return MTCmatrice;
}
