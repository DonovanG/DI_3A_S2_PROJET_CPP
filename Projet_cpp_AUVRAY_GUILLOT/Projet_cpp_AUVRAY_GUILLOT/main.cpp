/**
* \file main.cpp
* \brief fichier principale contenant la m�thode principale main
* \author{ GUILLOT Donovan, AUVRAY, Damien }
*\version 1
*/

#include <iostream>
#include "CException.h"
#include "CMatrice.h"
#include "CFile.h"

using namespace std;

/** \fn int main(int argc, char **argv)
* \brief Fonction principale
* Instancie autant de matrices qu'il y a de fichiers pass�s en param�tre
* effectue diff�rentes op�ration sur ces mattrices
* affiche le r�sultat dans la console pour chaque op�ration
* l�ve et affiche des exception si les op�ration ne peuvent pas �tre effectu�e
* \return int : 0 si tout c'est bien pass�
*/
int main(int argc, char **argv)
{	
	/** Verifie la presence d arguments */
	if(argc==1){
		try{
			throw CException(0, "Aucun argument saisi.");
		}catch (CException EXCErreur){
			cerr << EXCErreur << endl;
			return 0;
		}
	}
	
	CMatrice<double>* MTCmatrice = new CMatrice<double>[argc-1];
	string * pMTCNomMatrice = new string[argc-1];
	bool afficher = true;

	/** Lecture des matrices dans les fichiers + instanciation dans un tableau de matrice */
	for(unsigned int uiBoucle=1;uiBoucle<(unsigned)argc;uiBoucle++){
		MTCmatrice[uiBoucle-1];
		try{
			CFile file(argv[uiBoucle]);
			MTCmatrice[uiBoucle-1] = file.FILLireMatrice();
			pMTCNomMatrice[uiBoucle-1] = string(argv[uiBoucle]).substr(0, string(argv[uiBoucle]).find("."));
		}catch(CException EXCErreur){
			cerr << EXCErreur << endl;
			return 0;
		}
	}

	/** variables */
    double c = 0;
    cout << "Saisir une valeur c : ";
    cin >> c;
    while( cin.fail() || (cin.peek() != '\r' && cin.peek() != '\n')){
        cout << "Erreur, Veuillez saisir une valeur numerique: ";
        cin.clear();
        while( cin.get() != '\n' );
        cin >> c;
    }
	
	/** affichages des matrices */
    cout << "Afficher les matrices :"<<endl;
	for(unsigned int uiBoucle=1;uiBoucle<(unsigned)argc;uiBoucle++){
		try{
			cout << pMTCNomMatrice[uiBoucle-1] << " :" << endl;
			MTCmatrice[uiBoucle-1].MTCAfficher();
			cout << endl;
		}catch(CException EXCErreur){
			cerr << EXCErreur << endl;
		}
	}
	
	/** multiplication de chaque matrice par la constante et affichage */
	cout << "Multiplication par c :"<<endl;
	for(unsigned int uiBoucle=1;uiBoucle<(unsigned)argc;uiBoucle++){
		try{
			cout << pMTCNomMatrice[uiBoucle-1] << "*" << c << " :" << endl;
			(MTCmatrice[uiBoucle-1]*c).MTCAfficher();
			cout << endl;
		}catch(CException EXCErreur){
			cerr << EXCErreur << endl;
		}
	}

	/** division de chaque matrice par la constante et affichage 
	*	exception lev� si c = 0
	*/
	cout << "Division par c :"<<endl;
	for(unsigned int uiBoucle=1;uiBoucle<(unsigned)argc;uiBoucle++){
		try{
			cout << pMTCNomMatrice[uiBoucle-1] << "/" << c << " :" << endl;
			(MTCmatrice[uiBoucle-1]/c).MTCAfficher();
			cout << endl;
		}catch(CException EXCErreur){
			cerr << EXCErreur << endl;
		}
	}

	/** affichage de la transpos�e de chaque matrice */
	cout << "Transposees :"<<endl;
	for(unsigned int uiBoucle=1;uiBoucle<(unsigned)argc;uiBoucle++){
		try{
			cout << "T(" << pMTCNomMatrice[uiBoucle-1] << ") :" << endl;
			MTCmatrice[uiBoucle-1].MTCTransposee().MTCAfficher();
			cout << endl;
		}catch(CException EXCErreur){
			cerr << EXCErreur << endl;
		}
	}

	/** somme des matrices entre elles deux � deux et affichage du r�sultat */
	cout << "Additions matrices deux a deux :"<<endl;
	for(unsigned int uiBoucle1=1;uiBoucle1<(unsigned)argc;uiBoucle1++){
		for(unsigned int uiBoucle2=1;uiBoucle2<(unsigned)argc;uiBoucle2++){
			try{
				cout << pMTCNomMatrice[uiBoucle1-1] << "+" << pMTCNomMatrice[uiBoucle2-1] << " :" << endl;
				(MTCmatrice[uiBoucle1-1]+MTCmatrice[uiBoucle2-1]).MTCAfficher();
				cout << endl;
			}catch(CException EXCErreur){
				cerr << EXCErreur << endl;
			}
		}
	}

	/** soustraction des matrices entre elles deux � deux et affichage du r�sultat */
	cout << "Soustraction matrices deux a deux:"<<endl;
	for(unsigned int uiBoucle1=1;uiBoucle1<(unsigned)argc;uiBoucle1++){
		for(unsigned int uiBoucle2=1;uiBoucle2<(unsigned)argc;uiBoucle2++){
			try{
				cout << pMTCNomMatrice[uiBoucle1-1] << "-" << pMTCNomMatrice[uiBoucle2-1] << " :" << endl;
				(MTCmatrice[uiBoucle1-1]-MTCmatrice[uiBoucle2-1]).MTCAfficher();
				cout << endl;
			}catch(CException EXCErreur){
				cerr << EXCErreur << endl;
			}
		}
	}

	/** Addition des matrices entre elles et affichage du r�sultat */
	cout << "Additions de toutes les matrices :"<<endl;
	CMatrice<double> MTCmatriceAddition(MTCmatrice[0]);
	afficher = true;
	for(unsigned int uiBoucle=1;uiBoucle<(unsigned)argc;uiBoucle++){
		try{
			if(uiBoucle==1)
				cout << pMTCNomMatrice[uiBoucle-1];
			else{
				cout << "+" << pMTCNomMatrice[uiBoucle-1];
				MTCmatriceAddition = (MTCmatriceAddition+MTCmatrice[uiBoucle-1]);
			}
		}catch(CException EXCErreur){
			cout << " :" << endl;
			cerr << EXCErreur << endl;
			afficher = false;
		}
	}
	if(afficher){
		cout << " :" << endl;
		MTCmatriceAddition.MTCAfficher();
	}
	cout << endl;

	/** Addition et soustraction des matrices entre elles et affichage du r�sultat */
	cout << "Operation alternation soustraction puis addition de toutes les matrices :"<<endl;
	CMatrice<double> MTCmatriceAlternation(MTCmatrice[0]);
	afficher = true;
	for(unsigned int uiBoucle=1;uiBoucle<(unsigned)(argc);uiBoucle++){
		try{
			if(uiBoucle==1){
				cout << pMTCNomMatrice[uiBoucle-1];
			}else{
				if(uiBoucle%2==0){
					cout << "-" << pMTCNomMatrice[uiBoucle-1];
					MTCmatriceAlternation = MTCmatriceAlternation-MTCmatrice[uiBoucle-1];
				}
				else{
					cout << "+" << pMTCNomMatrice[uiBoucle-1];
					MTCmatriceAlternation = MTCmatriceAlternation+MTCmatrice[uiBoucle-1];
				}
				
			}
		}catch(CException EXCErreur){
			cout << " :" << endl;
			cerr << EXCErreur << endl;
			afficher = false;
		}
	}
	if(afficher){
		cout << " :" << endl;
		MTCmatriceAlternation.MTCAfficher();
	}
	cout << endl;

	/** multiplication des matrices entre elles deux � deux et affichage du r�sultat */
	cout << "Multiplication matrices deux a deux:"<<endl;
	for(unsigned int uiBoucle1=1;uiBoucle1<(unsigned)argc;uiBoucle1++){
		for(unsigned int uiBoucle2=1;uiBoucle2<(unsigned)argc;uiBoucle2++){
			try{
				cout << pMTCNomMatrice[uiBoucle1-1] << "*" << pMTCNomMatrice[uiBoucle2-1] << " :" << endl;
				(MTCmatrice[uiBoucle1-1]*MTCmatrice[uiBoucle2-1]).MTCAfficher();
				cout << endl;
			}catch(CException EXCErreur){
				cerr << EXCErreur << endl;
			}
		}
	}

	delete[] MTCmatrice;
	delete[] pMTCNomMatrice;
    return 0;
}
